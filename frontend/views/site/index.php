

/**
 * This file is part of mnemosyne.
 *
 * mnemosyne is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * mnemosyne is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details
 * 
 * You should have received a copy of the GNU General Public License
 * long with mnemosyne.  If not, see <https://www.gnu.org/licenses/>.
**/

<?php

/* @var $this yii\web\View */

# Include useful namespaces
use yii\helpers\Url;

# Set the title
$this->title = 'Mnemosyne @ ATLAS: UTSA Political Science Analysis Platform';

# Begin page content
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome to Mnemosyne(d) @ ATLAS</h1>

        <p class="lead">ATLAS is UNIX like environment for academic research.</p>
        <p class="lead">Mnemosyne(d) is an open sources analysis tool for researchers.</p>

        <p><a class="btn btn-lg btn-success" href="<?php echo Url::toRoute('site/signup'); ?>">Get started with Mnemosyne(d)</a></p>
        <!-- <p><a class="btn btn-lg btn-success" href="site/signup">Get started with ATLAS</a></p> -->

    </div>

    <div class="body-content">
    </div>
</div>
