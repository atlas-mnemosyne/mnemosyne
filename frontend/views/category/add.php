

/**
 * This file is part of mnemosyne.
 *
 * mnemosyne is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * mnemosyne is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details
 * 
 * You should have received a copy of the GNU General Public License
 * long with mnemosyne.  If not, see <https://www.gnu.org/licenses/>.
**/

<?php
/* @var $this yii\web\View */

# Use the URL namespace
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<h1>category/add</h1>

<?php
/*****************************************
* Add handle form
******************************************/
$addCategoryForm = ActiveForm::begin(
[
    'action' => ['category/add'],
    'id' => 'add-category-form',
    'options' => ['class' => 'form-horizontal'],
]);

?>
<div class="row">
    <div class="col-sm-6" style="width: 160px;">
        <?php echo $addCategoryForm->field($this->params['category'], 'name')->textInput(['style'=>'width:150px']); ?>
    </div>
    <div class="col-sm-6" style="width: 155px;">
        <?php echo $addCategoryForm->field($this->params['addHandleFormModel'], 'label')->textInput(['style'=>'width:150px']); ?>
    </div>
</div>



<div class="form-group">
      <!-- //Html::submitButton(Yii::t('app', 'add_handle'), ['class' => 'btn btn-primary']) -->
      <?= Html::submitButton('Add Handle', ['class' => 'btn btn-primary']) ?>
</div>
<?php
ActiveForm::end();
