

/**
 * This file is part of mnemosyne.
 *
 * mnemosyne is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * mnemosyne is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details
 * 
 * You should have received a copy of the GNU General Public License
 * long with mnemosyne.  If not, see <https://www.gnu.org/licenses/>.
**/

<?php

namespace frontend\controllers;

use Yii;
use \yii\web\View;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\helpers\Url;

class MapController extends \yii\web\Controller
{
    /**
     * Function handles access rules. Currently allows
     * only authenticated users to access any handle ops
     *
     * @author  Ben Shirani <ben.shirani@gmail.com>
     *
     * @since 1.0
     *
     * @return array Array of access rules.
     */
    public function behaviors()
    {
        return
        [
            'access' =>
            [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' =>
                [
                    [
                        'allow' => false,
                        'actions' => ['index'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        # Register CSS File for map
        \Yii::$app->getView()->registerCssFile(\Yii::$app->request->BaseUrl . '/css/map.css', ['position' => VIEW::POS_HEAD, 'depends' => [\yii\web\JqueryAsset::className()]]);

        # Register javascript for google maps
        \Yii::$app->getView()->registerJsFile(\Yii::$app->request->BaseUrl . '/js/MapIndex.js', ['position' => VIEW::POS_HEAD, 'depends' => [\yii\web\JqueryAsset::className()]]);
        // \Yii::$app->getView()->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyBz3nUkK8swwYcVY6wcR0_eODEDir8LMuo&callback=initMap', ['position' => VIEW::POS_END, 'depends' => [\yii\web\JqueryAsset::className()]]);
        \Yii::$app->getView()->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyBz3nUkK8swwYcVY6wcR0_eODEDir8LMuo&callback=initialize', ['position' => VIEW::POS_END, 'depends' => [\yii\web\JqueryAsset::className()]]);


        return $this->render('index');
    }

}
