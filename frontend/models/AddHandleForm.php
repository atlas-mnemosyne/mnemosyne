

/**
 * This file is part of mnemosyne.
 *
 * mnemosyne is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * mnemosyne is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details
 * 
 * You should have received a copy of the GNU General Public License
 * long with mnemosyne.  If not, see <https://www.gnu.org/licenses/>.
**/

<?php

namespace frontend\models;

class AddHandleForm extends \yii\base\Model
{
    public $handle;
    public $label;

    public function rules()
    {
        # Array of rules
        return
        [
          # Required to load
          [['AddHandleForm'], 'safe'],

          # These fields are required
          [['handle'], 'required'],

          # Handle must look like this:
          [['handle'], 'match', 'pattern' => '/^@?(\w){1,15}$/i'],

          # Labels are strings
          [['label'], 'string', 'max' => 155]
        ];
    }
}
