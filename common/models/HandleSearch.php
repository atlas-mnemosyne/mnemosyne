

/**
 * This file is part of mnemosyne.
 *
 * mnemosyne is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * mnemosyne is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details
 * 
 * You should have received a copy of the GNU General Public License
 * long with mnemosyne.  If not, see <https://www.gnu.org/licenses/>.
**/

<?php

# Define namespace for model
namespace common\models;

# Use other useful namespaces
use \Yii;
use common\models\Handle;
use common\models\XrefProjectUser;
use common\models\XrefProjectHandle;

/**
 * This is the model class for Handle searching".
 *
 * @property int $numTweets
 *
 */
class HandleSearch extends Handle
{
    # Number of tweets from this handle total
    public $numTweets;

  /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return
        [

            //[['user_id', 'handle'], 'required'],
            [['user_id', 'verified'], 'integer'],
            [['user_since', 'last_update', 'numTweets'], 'safe'],
            [['handle', 'name', 'profile_image'], 'string', 'max' => 255],
            [['label'], 'string', 'max' => 50],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
      * {@inheritdoc}
      */
     public function search($project_id, $params)
     {
         # Main query
         //$query = Handle::find()->where(['']);
         $subQuery = XrefProjectHandle::find()->select('handle_id')->where(['project_id' => $project_id]);

         $query = Handle::find()->where(['in', 'id', $subQuery]);

         # Data data provider
         $dataProvider = new \yii\data\ActiveDataProvider
         ([
             'query' => $query,
             'pagination' =>
             [
                 'pageSize' => 150,
              ],

         ]);

         # Sorting
         $dataProvider->setSort
         ([
             'attributes' =>
             [
                 'handle',
                 'label',
                 'name',
                 'user_since'
             ]
         ]);

         # Try to load in params
         if (!($this->load($params) && $this->validate()))
         {
             return $dataProvider;
         }

         # Handle
         $query->andFilterWhere(['like', 'handle', $this->handle]);

         # Label
         $query->andFilterWhere(['like', 'label', $this->label]);

         # Name
         $query->andFilterWhere(['like', 'name', $this->name]);

         # User Since
         $query->andFilterWhere(['<', 'user_since', $this->user_since]);

         # Return
         return $dataProvider;
     }
}
