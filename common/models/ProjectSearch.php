

/**
 * This file is part of mnemosyne.
 *
 * mnemosyne is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * mnemosyne is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details
 * 
 * You should have received a copy of the GNU General Public License
 * long with mnemosyne.  If not, see <https://www.gnu.org/licenses/>.
**/

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "project".
 *
 */
class ProjectSearch extends \common\models\Project
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name'
        ];
    }

    /**
     * {@inheritdoc}
     */
     public function search($params)
     {
         # Main query
         $query = Project::find();

         # Data data provider
         $dataProvider = new \yii\data\ActiveDataProvider
         ([
             'query' => $query,
             'pagination' =>
             [
                 'pageSize' => 150,
              ],

         ]);

         # Sorting
         $dataProvider->setSort
         ([
             'attributes' =>
             [
                 'id',
                 'user_id',
                 'name'
             ]
         ]);

         # Try to load in params
         if (!($this->load($params) && $this->validate()))
         {
             return $dataProvider;
         }

         # Handle
         $query->andFilterWhere(['like', 'name', $this->name]);

         # Label
         $query->andFilterWhere(['like', 'user_id', $this->user]);

         # Name
         $query->andFilterWhere(['like', 'id', $this->id]);

         # Return
         return $dataProvider;
     }
}
