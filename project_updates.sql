-- Set the project ID of categories
/*
UPDATE category SET project_id = (SELECT id FROM project WHERE name = 'Hofferberth UN');

-- Add foriegn key to projects table
ALTER TABLE category
ADD FOREIGN KEY (project_id) REFERENCES project(id);
*/

-- Add all handles to this project
INSERT INTO xref_project_handle (handle_id, project_id)
SELECT id, (SELECT id FROM project WHERE name = 'Hofferberth UN') FROM handle;

-- Set project id of all handles in xref_project_handle
UPDATE xref_project_handle SET project_id = (SELECT id FROM project WHERE name = 'Hofferberth UN');
